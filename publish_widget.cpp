/*
Copyright 2010 Robert Marmorstein <robert@narnia.homeunix.com>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of 
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

Portions of this code were automatically generated by kapptemplate 
which was written by Kevin Krammer <kevin@akonadi-project.org>
*/
#include <QPushButton>
#include <QMessageBox>
#include <KCanvasBase.h>
#include <KCanvasObserverBase.h>
#include <QGridLayout>
#include "publish_widget.h"
#include "publish_dialog.h"

PublishWidget::PublishWidget(QWidget* parent) : QDockWidget("Publish-To", parent), KCanvasObserverBase()
{
	QWidget* mainWidget = new QWidget(this);
	QGridLayout* layout = new QGridLayout(mainWidget);
	m_canvas = NULL;

	m_dialog = NULL;

	setWindowTitle("Publish");
	m_publish = new QPushButton(mainWidget);
	layout->addWidget(m_publish, 0, 0);
	m_publish->setText( "Publish To Calendar" );
	m_publish->setToolTip("Publish this document as an attachment to a Calendar event.");
	setWidget(mainWidget);
	connect(m_publish, SIGNAL(clicked()), this, SLOT(publish_document()));
}

PublishWidget::~PublishWidget(){
	delete m_publish;
}

void PublishWidget::publish_document(){
	if (m_dialog == NULL)
		m_dialog = new PublishDialog(NULL);
	if (m_canvas)
		m_dialog->setCanvas(m_canvas);
	m_dialog->show();	
}

void PublishWidget::setCanvas(KCanvasBase* canvas){
	m_canvas = canvas;
	if (m_dialog)
		m_dialog->setCanvas(canvas);
}
